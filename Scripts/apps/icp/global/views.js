
// global -> views


var NavView = Marionette.ItemView.extend({
    template: "#nav-template",
    events: {
        "click a" : "navigate"
    },
    navigate: function(ev){
        ev.preventDefault();
        var target = $(ev.currentTarget).attr("href");
        icpApp.AppRouter[target]();
        //icpApp.AppRouter.navigate(target, true);
    }
});