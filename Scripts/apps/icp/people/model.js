var staticPeopleData = [
    {first: "Chris", last: "Lindsey"},
    {first: "Jason", last: "Meckley"},
    {first: "Ben", last: "Sykes"},
    {first: "Dave", last: "Kopp"},
    {first: "Alex", last: "Eisenhart"},
    {first: "Matt", last: "Miller"}
];


var Person = Backbone.Model.extend({
    defaults: {
        "first": "John",
        "last": "Smith",
        "addresses": [{
            "street": "123 Somestreet",
            "city": "Somecity",
            "state": "PA",
            "zip": "12345",
            "country": "USA"
        }]
    }
});


var GroupOfPeople = Backbone.Collection.extend({
    model:Person
});


var people = new GroupOfPeople(staticPeopleData);