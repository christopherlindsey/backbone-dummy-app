var PeopleView = Marionette.ItemView.extend({

    template: false,
    collection: people,

    onRender: function(){
        
        this.kendoDataSource = new kendo.data.DataSource({
            data: this.collection.toJSON()
        });
        
        this.kendoGrid = this.$el.kendoGrid({
            dataSource: this.kendoDataSource,
            columns: [
                "first",
                "last"
            ],
            detailTemplate: _.template('<div class="detail-container"></div>'),
            detailInit: this.showUserDetails
        }).data("kendoGrid");
        
        this.listenTo(this.collection, 'add', this.updateGridData);
        this.listenTo(this.collection, 'change', this.updateGridData);
    },

    showUserDetails: function(e){
        console.log(e.data.address);
        e.detailRow.find(".detail-container").kendoGrid({
            dataSource: e.data.addresses,
            columns: [
                "street",
                "city",
                "state",
                "zip",
                "country"
            ]
        });
    },

    updateGridData: function(){
        this.kendoDataSource.data(this.collection.toJSON());
    }
});